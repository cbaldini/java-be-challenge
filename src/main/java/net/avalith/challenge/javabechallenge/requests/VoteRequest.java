package net.avalith.challenge.javabechallenge.requests;

import java.util.Date;
import lombok.Data;
import net.avalith.challenge.javabechallenge.entities.Area;
import net.avalith.challenge.javabechallenge.entities.Candidate;
import net.avalith.challenge.javabechallenge.entities.User;

@Data
public class VoteRequest {

    private Area area;
    private User user;
    private Candidate candidate;
    private String comment;
    private Date month;
}

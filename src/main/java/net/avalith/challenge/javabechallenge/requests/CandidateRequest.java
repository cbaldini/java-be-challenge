package net.avalith.challenge.javabechallenge.requests;

import lombok.Builder;
import lombok.Data;
import net.avalith.challenge.javabechallenge.entities.User;

@Data
@Builder
public class CandidateRequest {
    private Long id;
    private User user;
    private String firstName;
    private String lastName;
    private String cuit;

}

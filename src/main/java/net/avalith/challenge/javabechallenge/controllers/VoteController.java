package net.avalith.challenge.javabechallenge.controllers;

import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import net.avalith.challenge.javabechallenge.entities.Vote;
import net.avalith.challenge.javabechallenge.exceptions.ForbiddenException;
import net.avalith.challenge.javabechallenge.exceptions.UnauthorizedException;
import net.avalith.challenge.javabechallenge.requests.VoteRequest;
import net.avalith.challenge.javabechallenge.responses.VoteResponse;
import net.avalith.challenge.javabechallenge.services.UserService;
import net.avalith.challenge.javabechallenge.services.VoteService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

@AllArgsConstructor
@Controller
@RequestMapping("/votes")
public class VoteController {

    private final VoteService voteService;
    private final UserService userService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<VoteResponse>> getVotes (
        @RequestHeader(name = "Authorization") String token) {
            if (!userService.verifyRole(token, "ADMIN")) {
                throw new ForbiddenException();
            }
        return ResponseEntity.ok(voteService.getVotes());
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Vote> createVote(
        @RequestHeader(name = "Authorization") String token,
        @Valid @RequestBody VoteRequest request) {
        if (!userService.verifyRole(token, "ADMIN") && (!userService.verifyRole(token, "EMPLOYEE"))) {
            throw new UnauthorizedException();
        }
        System.out.println("ID DE USUARIO QUE VOTA: " + (userService.verifyId(token)).toString() + "########################################");
        System.out.println("USUARIO VOTADO: " + voteService.findVoteByUserCandidateAndArea(
            request.getCandidate().getId(),
            userService.verifyId(token),
            request.getArea().getId()
        ).toString());
        if((voteService.findVoteByUserCandidateAndArea(
            request.getCandidate().getId(),
            userService.verifyId(token),
            request.getArea().getId())).isPresent()){
            throw new UnauthorizedException();
        }
        else{return ResponseEntity.ok(voteService.createVote(request));}

    }
}

package net.avalith.challenge.javabechallenge.controllers;

import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import net.avalith.challenge.javabechallenge.entities.Role;
import net.avalith.challenge.javabechallenge.exceptions.ForbiddenException;
import net.avalith.challenge.javabechallenge.requests.RoleRequest;
import net.avalith.challenge.javabechallenge.services.UserService;
import net.avalith.challenge.javabechallenge.services.RoleService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

@AllArgsConstructor
@Controller
@RequestMapping("/roles")
public class RoleController {

    private final RoleService roleService;
    private final UserService userService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Role>> getAll(@RequestHeader(name = "Authorization") String token) {
        if (!userService.verifyRole(token, "ADMIN")) {
            throw new ForbiddenException();
        }

        return ResponseEntity.ok(roleService.getRoles());
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Role> create(
        @RequestHeader(name = "Authorization") String token,
        @Valid @RequestBody RoleRequest request) {
        if (!userService.verifyRole(token, "ADMIN")) {
            throw new ForbiddenException();
        }

        return ResponseEntity.ok(roleService.createRole(request));
    }

    @DeleteMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity<Role> delete(
        @RequestHeader(name = "Authorization") String token,
        @PathVariable("id") Long id) {
        if (!userService.verifyRole(token, "ADMIN")) {
            throw new ForbiddenException();
        }

        return ResponseEntity.ok(roleService.deleteRole(id));
    }
}

package net.avalith.challenge.javabechallenge.controllers;

import javax.validation.Valid;
import lombok.AllArgsConstructor;
import net.avalith.challenge.javabechallenge.exceptions.ForbiddenException;
import net.avalith.challenge.javabechallenge.requests.LoginRequest;
import net.avalith.challenge.javabechallenge.requests.PasswordUpdateRequest;
import net.avalith.challenge.javabechallenge.requests.RegisterRequest;
import net.avalith.challenge.javabechallenge.responses.AuthUserResponse;
import net.avalith.challenge.javabechallenge.responses.UserResponse;
import net.avalith.challenge.javabechallenge.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

@AllArgsConstructor
@Controller
@RequestMapping
public class AuthenticationController {

    private final UserService userService;

    @PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
    public ResponseEntity<AuthUserResponse> authenticate(@Valid @RequestBody LoginRequest request) {
        return ResponseEntity.ok(userService.authenticate(request));
    }

    @PostMapping(path = "/register", consumes = "application/json", produces = "application/json")
    public ResponseEntity<UserResponse> register(
        @RequestHeader(name = "Authorization") String token,
        @Valid @RequestBody RegisterRequest request) {
        if (!userService.verifyRole(token, "ADMIN")) {
            throw new ForbiddenException();
        }

        return ResponseEntity.ok(userService.register(request));
    }

    @GetMapping(path = "/me", produces = "application/json")
    public ResponseEntity<AuthUserResponse> me(@RequestHeader(name = "Authorization") String token) {
        return ResponseEntity.ok(userService.me(token));
    }

    @PostMapping(path = "/change-password", consumes = "application/json", produces = "application/json")
    public ResponseEntity recover(@RequestHeader(name = "Authorization") String token,
                                  @Valid @RequestBody PasswordUpdateRequest request) {
        return ResponseEntity.ok(userService.changePassword(token, request));
    }
}


package net.avalith.challenge.javabechallenge.responses;

import lombok.Builder;
import lombok.Data;
import net.avalith.challenge.javabechallenge.entities.Area;

@Data
@Builder
public class AreaResponse {
    private Long id;
    private String area;

    public static AreaResponse fromEntity(Area area){
        return AreaResponse.builder()
            .id(area.getId())
            .area(area.getArea())
            .build();
    }
}

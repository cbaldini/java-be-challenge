package net.avalith.challenge.javabechallenge.services;

import java.util.List;
import lombok.AllArgsConstructor;
import net.avalith.challenge.javabechallenge.entities.Area;
import net.avalith.challenge.javabechallenge.reporitories.AreaRepository;
import net.avalith.challenge.javabechallenge.requests.AreaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AreaService {

    @Autowired
    private final AreaRepository areaRepository;

    public List<Area> getAreas(){

        return areaRepository.findAll();
    }
    public Area createArea(AreaRequest request){

        Area area = Area.builder().area(request.getArea()).build();
        return areaRepository.save(area);

    }
}

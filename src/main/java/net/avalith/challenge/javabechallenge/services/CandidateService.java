package net.avalith.challenge.javabechallenge.services;

import java.util.List;
import lombok.AllArgsConstructor;
import net.avalith.challenge.javabechallenge.entities.Candidate;
import net.avalith.challenge.javabechallenge.reporitories.CandidateRepository;
import net.avalith.challenge.javabechallenge.responses.CandidateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CandidateService {

    @Autowired
    private final CandidateRepository candidateRepository;

    public List<Candidate> getCandidates(){

        return candidateRepository.findAll();
    }

    public CandidateResponse createCandidate(Candidate candidate){

        Candidate saved = candidateRepository.save(candidate);
        return CandidateResponse.fromEntity(saved);
    }
}
